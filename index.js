// eslint-disable-next-line spaced-comment
/**!
 * react-each v2.0.1
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-each/blob/master/LICENSE)
 */

import React from 'react';
import PropTypes from 'prop-types';

import aguid from 'aguid';
import valyou from 'valyou';
import { callIfDefinedNull } from 'call-if-defined';



export default class Each extends React.Component {

  constructor(...args) {
    super(...args);

    this._guid = aguid();
  }



  render() {
    const items = valyou(this.props.items);

    if (items == null || items.length === 0) {
      return callIfDefinedNull(this.props.else);
    }

    return React.createElement(
      React.Fragment,
      { key: this._guid },
      _renderChildren(items, this.props.renderItem, this.props.startIndex, this.props.endIndex)
    );
  }
}


Each.propTypes = {
  items:      PropTypes.oneOfType([PropTypes.array, PropTypes.func]),
  renderItem: PropTypes.func.isRequired,
  else:       PropTypes.func,

  startIndex: PropTypes.number,
  endIndex:   PropTypes.number
};



// region Private Render Functions

/** @private */
function _renderChildren(items, renderItem, startIndex, endIndex) {
  const children = [];
  const info     = { itemsLastIndex: (items.length - 1) };

  const finalStartIndex = ((startIndex == null || startIndex < 0) ? 0 : startIndex);
  const finalEnd        = ((endIndex == null || (endIndex > items.length - 1)) ? items.length : (endIndex + 1));

  for (let i = finalStartIndex; i < finalEnd; i++) {
    children.push(renderItem(items[i], i, info, items));
  }

  return children;
}

// endregion
